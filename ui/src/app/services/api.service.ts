import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { serverData } from '../models/serverModel';
import { APIUrl } from './constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient
  ) { }

  get(serverId: string = ""): Observable<serverData> {
    return this.http.get<serverData>(`${APIUrl}/${serverId}`)
  }

  putServer(serverId, desirableStatus) {
    return this.http
      .put(`${APIUrl}/${serverId}/${desirableStatus}`, {
        "status": desirableStatus
      })
  }
}
// I used an Observable just to havea possibility to subscribe to this method and get the data.