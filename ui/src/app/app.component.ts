import { Component, OnInit } from '@angular/core';

import { serverData } from './models/serverModel';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './server/server.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private apiService: ApiService) { }
  public servers;

  ngOnInit() {
    this.apiService.get().subscribe((res: serverData) => {
      this.servers = res;
    })
  }
}
