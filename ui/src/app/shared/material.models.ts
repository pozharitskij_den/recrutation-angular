import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';

import { NgModule } from '@angular/core';


@NgModule({
  declarations: [],
  exports: [
    MatMenuModule,
    MatIconModule
  ]
})
export class MaterialModule { }