import { Component, Input } from '@angular/core';
import { combineLatest, interval } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { ApiService } from '../services/api.service';
import { serverData } from '../models/serverModel';

@Component({
    selector: 'app-server',
    templateUrl: './server.component.html',
    styleUrls: ['./server.component.scss']
})

export class ServerComponent {
    constructor(private apiService: ApiService) { }
    @Input() serverData: serverData;

    manageServer(option: string): void {
        combineLatest(this.apiService.putServer(this.serverData.id, option), this.apiService.get(this.serverData.id)).subscribe((res: any) => {
            this.serverData = res[0]
        })
    }
    rebootServer(): void {
        this.apiService.putServer(this.serverData.id, 'reboot').subscribe((response: serverData) => {
            this.serverData = response;
        });
        interval(1000)
            .pipe(takeWhile(() => this.serverData.status.localeCompare("ONLINE") !== 0))
            .subscribe(() => this.apiService.get(this.serverData.id)
                .subscribe((res: serverData) => this.serverData.status = res.status))
    }
}

/* I used interval rxjs's method just because it is really convenient, instead of use setInterval, a fortiori it has GREAT method - takeWhile to break the loop. WOW!
    combineLatest... You know, I am React-boy, but Angular is ok and I want to grow in Angular, I used to write some apps using Redux-thunk and this middleware do similar things = async call of these two methods.'
*/
