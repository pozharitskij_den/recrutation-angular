import { ApiService } from '../services/api.service';
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { of } from 'rxjs';

import { ServerComponent } from '../server/server.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../shared/material.models';

describe('ServerComponent', () => {
  let component: ServerComponent;
  let fixture: ComponentFixture<ServerComponent>;
  let apiService: ApiService;
  let spy: jasmine.Spy;
  let mockServer;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, MaterialModule],
      declarations: [ServerComponent],
      providers: [ApiService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerComponent);
    component = fixture.componentInstance;
    apiService = fixture.debugElement.injector.get(ApiService);
    mockServer = { id: '1', name: 'Mock Server', status: "ONLINE" }
    spy = spyOn(apiService, 'putServer').and.returnValue(of(mockServer));
    component.serverData = mockServer;
    fixture.detectChanges();
  });

  it('should be create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Servers', () => {
    expect(component.serverData).toBeTruthy();
  });

  it('should call putServer from apiService', () => {
    component.rebootServer();
    expect(spy.calls.any()).toBeTruthy();
  })

  it('should set serverData (if set => data was got)', () => {
    component.rebootServer();
    expect(component.serverData).toEqual(mockServer);
  })

})