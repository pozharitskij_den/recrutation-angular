import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';


import { AppComponent } from './app.component';
import { MaterialModule } from './shared/material.models';
import { ServerComponent } from './server/server.component';
import { SearchPipe } from './services/search.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MaterialModule,
    NoopAnimationsModule,
    FormsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
